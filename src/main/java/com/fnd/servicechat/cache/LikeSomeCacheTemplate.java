package com.fnd.servicechat.cache;

import com.fnd.servicechat.entity.UserMsg;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @Description 聊天内容临时存储  TODO 后续放在mongodb上
 * @Author Administrator
 * @Date 2019/11/13 11:54
 * @Version 1.0
 */
@Component
public class LikeSomeCacheTemplate {
    private Set<UserMsg> SomeCache = new LinkedHashSet<>();

    public void save(Object user,Object msg){
        UserMsg userMsg = new UserMsg();
        userMsg.setName(String.valueOf(user));
        userMsg.setMsg(String.valueOf(msg));
        SomeCache.add(userMsg);
    }

    public Set<UserMsg> cloneCacheMap(){
        return SomeCache;
    }

    public void clearCacheMap(){
        SomeCache.clear();
    }
}

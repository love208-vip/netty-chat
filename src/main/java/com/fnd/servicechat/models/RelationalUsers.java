package com.fnd.servicechat.models;

import java.io.Serializable;

public class RelationalUsers implements Serializable {

    private Integer rId;

    private Integer uId;


    private Integer uIdN;


    private Integer rType;


    private static final long serialVersionUID = 1L;

    public Integer getrId() {
        return rId;
    }

    public void setrId(Integer rId) {
        this.rId = rId;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Integer getuIdN() {
        return uIdN;
    }

    public void setuIdN(Integer uIdN) {
        this.uIdN = uIdN;
    }

    public Integer getrType() {
        return rType;
    }

    public void setrType(Integer rType) {
        this.rType = rType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", rId=").append(rId);
        sb.append(", uId=").append(uId);
        sb.append(", uIdN=").append(uIdN);
        sb.append(", rType=").append(rType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
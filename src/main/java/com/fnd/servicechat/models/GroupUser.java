package com.fnd.servicechat.models;

import java.io.Serializable;

/**
 * @author 
 */
public class GroupUser implements Serializable {
    private Integer guId;

    /**
     * 群ID
     */
    private Integer gId;

    /**
     * 用户ID
     */
    private Integer uId;

    /**
     * 是否退群0:已退；1:在群
     */
    private Integer uType;

    private static final long serialVersionUID = 1L;

    public Integer getGuId() {
        return guId;
    }

    public void setGuId(Integer guId) {
        this.guId = guId;
    }

    public Integer getgId() {
        return gId;
    }

    public void setgId(Integer gId) {
        this.gId = gId;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Integer getuType() {
        return uType;
    }

    public void setuType(Integer uType) {
        this.uType = uType;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        GroupUser other = (GroupUser) that;
        return (this.getGuId() == null ? other.getGuId() == null : this.getGuId().equals(other.getGuId()))
            && (this.getgId() == null ? other.getgId() == null : this.getgId().equals(other.getgId()))
            && (this.getuId() == null ? other.getuId() == null : this.getuId().equals(other.getuId()))
            && (this.getuType() == null ? other.getuType() == null : this.getuType().equals(other.getuType()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getGuId() == null) ? 0 : getGuId().hashCode());
        result = prime * result + ((getgId() == null) ? 0 : getgId().hashCode());
        result = prime * result + ((getuId() == null) ? 0 : getuId().hashCode());
        result = prime * result + ((getuType() == null) ? 0 : getuType().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", guId=").append(guId);
        sb.append(", gId=").append(gId);
        sb.append(", uId=").append(uId);
        sb.append(", uType=").append(uType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
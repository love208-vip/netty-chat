package com.fnd.servicechat.dao;

import com.fnd.servicechat.models.RelationalUsers;

import java.util.List;


public interface RelationalUsersMapper {

    int deleteByPrimaryKey(Integer rId);

    int insert(RelationalUsers record);

    RelationalUsers selectByPrimaryKey(Integer rId);

    List<RelationalUsers> selectAll();

    int updateByPrimaryKey(RelationalUsers record);

    // 新增用户关系
    int addRelationalUser(RelationalUsers record);
}
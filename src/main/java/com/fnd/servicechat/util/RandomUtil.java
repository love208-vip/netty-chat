package com.fnd.servicechat.util;

import java.util.Random;
import java.util.UUID;

/**
 * @Description 随机命名工具类
 * @Author Administrator
 * @Date 2019/11/13 11:41
 * @Version 1.0
 */
public class RandomUtil {
    private static Random ran = new Random();
    private final static int delta = 0x9fa5 - 0x4e00 + 1;

    public static char getName(){
        return (char)(0x4e00 + ran.nextInt(delta));
    }

    /**
     * 获取随机字符串(不重复)
     * @return
     */
    public static String getRandomUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }
}

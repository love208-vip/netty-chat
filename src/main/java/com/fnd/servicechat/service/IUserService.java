package com.fnd.servicechat.service;

import com.fnd.servicechat.models.Group;
import com.fnd.servicechat.models.UUser;

import java.util.List;
import java.util.Map;

/**
 * @Description 用户操作接口
 * @ClassName IUserService
 * @Author Administrator
 * @Data 2019/11/13 23:34
 * @Version 1.0
 */
public interface IUserService {
    /**
     * 用户登录
     * @param uName 用户名
     * @param uPwd 密码
     * @return
     *      null-失败 uPwd-成功
     */
    UUser login(String uName, String uPwd);

    /**
     * 校验用户是否存在
     * @param id
     * @return
     */
    boolean isExitByUserId(Integer id);

    /**
     * 根据用户的群组id获取该群组所有用户信息
     * @param groudId 群组id
     * @return
     */
    List<Integer> getAllUserIdsByGroudId(Integer groudId);

    /**
     * 获取所有用户
     * @return
     */
    List<UUser> getAllUser(String id);

    /**
     * 根据用户id获取其所有群组信息
     * @param id
     * @return
     */
    List<Map> getGroupByUserId(Integer id);
}

package com.fnd.servicechat.service;

import com.fnd.servicechat.models.GroupUser;
import com.fnd.servicechat.models.RelationalUsers;

public interface RelationalUsersService {

    // 新增用户关系
    int addRelationalUser(RelationalUsers record);

    // 用户进群
    int userAddGroup(GroupUser groupUser);
}

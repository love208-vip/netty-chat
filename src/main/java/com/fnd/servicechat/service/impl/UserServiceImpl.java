package com.fnd.servicechat.service.impl;

import com.fnd.servicechat.dao.GroupUserMapper;
import com.fnd.servicechat.dao.UUserMapper;
import com.fnd.servicechat.models.GroupUser;
import com.fnd.servicechat.models.GroupUserExample;
import com.fnd.servicechat.models.UUser;
import com.fnd.servicechat.models.UUserExample;
import com.fnd.servicechat.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description 用户处理类实现
 * @ClassName UserServiceImpl
 * @Author Administrator
 * @Data 2019/11/13 23:34
 * @Version 1.0
 */
@Service
public class UserServiceImpl implements IUserService{
    @Autowired
    private UUserMapper uUserMapper; //用户实体mapper

    @Autowired
    private GroupUserMapper groupUserMapper;  //群组实体mapper
    @Override
    public UUser login(String uName, String uPwd) {
        UUserExample uUserExample = new UUserExample();
        uUserExample.createCriteria().andUNameEqualTo(uName).andUPwdEqualTo(uPwd);
        List<UUser> users = uUserMapper.selectByExample(uUserExample);
        if(null != users && users.size() >= 0){
            return users.get(0);
        }else{
            return null;
        }
    }

    @Override
    public boolean isExitByUserId(Integer id) {
        return null != uUserMapper.selectByPrimaryKey(id) ? true : false;
    }

    @Override
    public List<Integer> getAllUserIdsByGroudId(Integer groudId) {
        GroupUserExample groupUserExample = new GroupUserExample();
        groupUserExample.createCriteria().andGIdEqualTo(groudId);
        List<GroupUser> list = groupUserMapper.selectByExample(groupUserExample);
        if(null != list && list.size() > 0){
           return list.stream().map(GroupUser::getuId).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<UUser> getAllUser(String id) {
        return uUserMapper.selectAllUser(id);
    }

    /**
     * 根据用户id获取其所有群组信息
     *
     * @param id
     * @return
     */
    @Override
    public List<Map> getGroupByUserId(Integer id) {
        return groupUserMapper.getGroupByUserId(id);
    }
}

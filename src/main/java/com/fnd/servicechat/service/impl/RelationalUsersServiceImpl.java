package com.fnd.servicechat.service.impl;

import com.fnd.servicechat.dao.GroupUserMapper;
import com.fnd.servicechat.dao.RelationalUsersMapper;
import com.fnd.servicechat.models.GroupUser;
import com.fnd.servicechat.models.RelationalUsers;
import com.fnd.servicechat.service.RelationalUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RelationalUsersServiceImpl implements RelationalUsersService {

    @Autowired
    private RelationalUsersMapper relationalUsersMapper;

    @Autowired
    private GroupUserMapper groupUserMapper;


    @Override
    @Transactional(rollbackFor=Exception.class)
    public int addRelationalUser(RelationalUsers record) {

        RelationalUsers users = new RelationalUsers();
        users.setuId(record.getuIdN());
        users.setuIdN(record.getuId());
        users.setrType(1);
        record.setrType(1);
        relationalUsersMapper.addRelationalUser(users);
        relationalUsersMapper.addRelationalUser(record);
        if(relationalUsersMapper.addRelationalUser(users) > 0 && relationalUsersMapper.addRelationalUser(record) > 0){
            return 1;
        }
        return 0;
    }

    @Override
    public int userAddGroup(GroupUser groupUser) {
        return groupUserMapper.insert(groupUser);
    }
}

package com.fnd.servicechat.service;

import com.fnd.servicechat.controller.dto.Result;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description 文件操作接口
 * @Author Administrator
 * @Date 2019/11/15 14:53
 * @Version 1.0
 */
public interface IFileOptionService {
    /**
     * 用户上传文件
     * @param file
     * @return
     *      上传结果 true-成功 false-失败
     */
    Result upload(MultipartFile file);
}

package com.fnd.servicechat.task;

import com.fnd.servicechat.cache.LikeSomeCacheTemplate;
import com.fnd.servicechat.entity.UserMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.Future;

/**
 * @Description 异步任务处理
 * @Author Administrator
 * @Date 2019/11/13 11:57
 * @Version 1.0
 */
@Component
public class MsgAsyncTesk {
    @Autowired
    private LikeSomeCacheTemplate cacheTemplate;

//    @Autowired
//    private UserMsgRepository userMsgRepository; // TODO  用来存储用户信息 TODO 通过mq存储在 mongodb中

    @Async
    public Future<Boolean> saveChatMsgTask() throws Exception{
//        System.out.println("启动异步任务");
        Set<UserMsg> set = cacheTemplate.cloneCacheMap();
        for (UserMsg item:set){
            //保存用户消息 TODO
//            userMsgRepository.save(item);
        }
        //清空临时缓存
        cacheTemplate.clearCacheMap();
        return new AsyncResult<>(true);
    }
}

package com.fnd.servicechat;

import com.fnd.servicechat.ws.NettyConfig;
import com.fnd.servicechat.ws.TCPServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@MapperScan("com.fnd.servicechat.dao")
public class ServicechatApplication {

    public static void main(String[] args) throws Exception{
        ConfigurableApplicationContext context = SpringApplication.run(ServicechatApplication.class, args);
        //注入NettyConfig 获取对应Bean
        NettyConfig nettyConfig = context.getBean(NettyConfig.class);
        //注入TCPServer 获取对应Bean
        TCPServer tcpServer = context.getBean(TCPServer.class);
        //启动websocket的服务
        tcpServer.start();
    }
}

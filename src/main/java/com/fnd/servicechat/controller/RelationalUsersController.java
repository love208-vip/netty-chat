package com.fnd.servicechat.controller;


import com.fnd.servicechat.models.GroupUser;
import com.fnd.servicechat.models.RelationalUsers;
import com.fnd.servicechat.service.RelationalUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class RelationalUsersController {

    @Autowired
    private RelationalUsersService relationalUsersService;


    /**
     * 新增用户关系
     * @param relationalUsers
     * @return
     */
    @RequestMapping("/addRelationalUser")
    public String addRelationalUser(RelationalUsers relationalUsers){
        return relationalUsersService.addRelationalUser(relationalUsers)+"";
    }

    /**
     * 用户加群
     * @return
     */
    @RequestMapping("/userAddGroup")
    public String userAddGroup(GroupUser groupUser){
        return relationalUsersService.userAddGroup(groupUser)+"";
    }
}

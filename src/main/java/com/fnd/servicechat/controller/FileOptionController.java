package com.fnd.servicechat.controller;

import com.fnd.servicechat.controller.dto.Result;
import com.fnd.servicechat.service.IFileOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @Description 文件操作Controller
 * @Author Administrator
 * @Date 2019/11/15 14:51
 * @Version 1.0
 */
@Controller
@RequestMapping("/file")
public class FileOptionController {
    @Autowired
    private IFileOptionService fileOptionService;
    // TODO 后续优化 返回对象
    @RequestMapping(value = "/upload", method = POST)
    @ResponseBody
    public Result upload(
            @RequestParam(value = "file", required = true) MultipartFile file) {
        return fileOptionService.upload(file);
    }
}

package com.fnd.servicechat.ws.messaging;

/**
 * @Description TODO
 * @Author Administrator
 * @Date 2019/11/13 14:16
 * @Version 1.0
 */
public class SendMessage<T> extends Message<T> {
    //用户ID
    private Integer userId;
    //用户名
    private String userName;
    //消息类型  1; //客服登录 3; //客服登出 7; // 聊天 8;  //文件传输
    private Integer type;
    //源类型 100-点对点 101-群聊
    private Integer messageType;
    //messageType=100时发送的对方的userid
    private Integer toUserId;
    //messageType=101时发送的群组的id
    private Integer groupId;

    public static final int TYPE_CUSTOMER_SERVICE_LOGIN = 1; //登录
    public static final int TYPE_CUSTOMER_SERVICE_LOGOUT = 3; //退出
    public static final int TYPE_SAY = 7; // 聊天
    public static final int TYPE_FILE = 8;  //文件传输
    public static final int MESSAGETYPE_POINT_TO_POINT = 100;  //点对点
    public static final int MESSAGETYPE_POINT_TO_GROUP = 101;  //群聊

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getMessageType() {
        return messageType;
    }

    public void setMessageType(Integer messageType) {
        this.messageType = messageType;
    }

    public Integer getToUserId() {
        return toUserId;
    }

    public void setToUserId(Integer toUserId) {
        this.toUserId = toUserId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }
}

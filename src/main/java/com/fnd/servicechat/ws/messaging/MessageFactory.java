package com.fnd.servicechat.ws.messaging;

import com.alibaba.fastjson.JSON;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

/**
 * @Description 消息工厂类
 * @Author Administrator
 * @Date 2019/11/15 8:58
 * @Version 1.0
 */
public class MessageFactory {
    /**
     * 生成 TextWebSocketFrame
     * @param message 发送的消息内容
     * @return
     */
    public static TextWebSocketFrame buildMessage(Message message){
        return new TextWebSocketFrame(JSON.toJSONString(message));
    }
}

package com.fnd.servicechat.ws.messaging;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * @Description 文件传输对象
 * @Author Administrator
 * @Date 2019/11/19 16:49
 * @Version 1.0
 */
public class MFile {
    private String originalFilename; //文件名称
    private String fileUrl; //文件在服务器端的路径
    private String fileSize; //文件大小
    public MFile(){}
    public MFile(String originalFilename, String fileUrl, String fileSize){
        this.originalFilename = originalFilename;
        this.fileUrl = fileUrl;
        this.fileSize = fileSize;
    }
    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }
}

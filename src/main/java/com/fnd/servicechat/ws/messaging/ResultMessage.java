package com.fnd.servicechat.ws.messaging;

/**
 * @Description 返回消息
 * @Author Administrator
 * @Date 2019/11/13 14:21
 * @Version 1.0
 */
public class ResultMessage<T> extends Message<T> {
    //消息类型
    private Integer type;
    /**
     * type
     */
    public static final int RESULT_Type_LOGIN_SUCCESS = 1; // 登录成功返回
    public static final int RESULT_Type_MESSAGE_SENDING = 2;  //对方发送消息成功
    public static final int RESULT_Type_MESSAGE_SERVICE = 3; //服务端通知消息
    public static final int RESULT_TYPE_MESSAGE_GROUPSENDING = 4;  //群组发送消息成功
    public static final int RESULT_TYPE_FILEMESSAGE_SENDING = 5;  //对方发送文件成功
    public static final int RESULT_TYPE_FILEMESSAGE_GROUPING = 6;  //群组发送文件成功

    public Integer fromUserId;  //消息发送者 type=2或4时返回
    public Integer groupId;     //群组id     type=4时返回
    public Integer getType() {
        return type;
    }

    public ResultMessage setType(Integer type) {
        this.type = type;
        return this;
    }

    public ResultMessage(){}
    public ResultMessage(int type){
        this.type = type;
    }
    public ResultMessage(int type, int fromUserId, T data){
        this.type = type;
        this.fromUserId = fromUserId;
        this.setData(data);
    }
    public ResultMessage(int type, T data){
        this.type = type;
        this.setData(data);
    }

    public Integer getFromUserId() {
        return fromUserId;
    }

    public ResultMessage setFromUserId(Integer fromUserId) {
        this.fromUserId = fromUserId;
        return this;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public ResultMessage setGroupId(Integer groupId) {
        this.groupId = groupId;
        return this;
    }
}

package com.fnd.servicechat.ws.messaging;

/**
 * @Description 消息对象
 * @Author Administrator
 * @Date 2019/11/13 14:12
 * @Version 1.0
 */
public class Message<T> {
    private T data;

    public Message(){
    }
    public Message(T data){
        this.data = data;
    }
    public T getData() {
        return data;
    }

    public Message setData(T data) {
        this.data = data;
        return this;
    }
}

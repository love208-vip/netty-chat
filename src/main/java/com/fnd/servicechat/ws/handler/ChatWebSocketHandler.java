package com.fnd.servicechat.ws.handler;

import com.fnd.servicechat.ws.messaging.SendMessage;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author Administrator
 * @Date 2019/11/15 13:14
 * @Version 1.0
 */
@Component
@Qualifier("chatWebSocketHandler")
@ChannelHandler.Sharable
public class ChatWebSocketHandler extends BaseWebSocketHandler{
    private final Logger logger = LoggerFactory.getLogger(ChatWebSocketHandler.class);
    @Autowired
    private MessageHandler messageHandler;  //消息处理类
    @Override
    protected void onMessage(SendMessage message, Channel channel) {
        Integer type = message.getType();
        logger.debug("会话id:" + channel.id().asLongText() + "，消息类型:" + type);
        //先前校验 TODO
        switch (type){
            case SendMessage.TYPE_CUSTOMER_SERVICE_LOGIN:  //登录操作
                messageHandler.login(message, channel);
                break;
            case SendMessage.TYPE_CUSTOMER_SERVICE_LOGOUT:  //退出操作
                messageHandler.quit(message.getUserId());
                break;
            case SendMessage.TYPE_SAY:  //聊天模式
                messageHandler.message(message, channel);
                break;
            case SendMessage.TYPE_FILE:  //文件上传模式
                messageHandler.fileMessage(message, channel);
                break;
        }
    }

    @Override
    protected void shutdownChannel(String clientId) {
        messageHandler.removeChannel(clientId);
    }
}

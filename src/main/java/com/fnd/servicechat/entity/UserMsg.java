package com.fnd.servicechat.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description TODO
 * @Author Administrator
 * @Date 2019/11/13 11:49
 * @Version 1.0
 */

public class UserMsg implements Serializable {

    private static final long serialVersionUID = 4133316147283239759L;

    private Integer id;

    private String name;

    private String msg;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
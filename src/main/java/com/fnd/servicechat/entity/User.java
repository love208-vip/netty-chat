package com.fnd.servicechat.entity;

import lombok.Data;

/**
 * 用户实体
 */
@Data
public class User {

    private Integer id;
    private String uname;
    private String upwd;
    private String phone;
    private Integer sex;
    private String uurl;
}

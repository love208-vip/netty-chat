/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50562
Source Host           : 127.0.0.1:3306
Source Database       : chat

Target Server Type    : MYSQL
Target Server Version : 50562
File Encoding         : 65001

Date: 2019-11-19 10:44:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `g_id` int(11) NOT NULL AUTO_INCREMENT,
  `g_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '群名',
  `u_id` int(11) DEFAULT NULL COMMENT '创建用户ID',
  PRIMARY KEY (`g_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES ('1', 'JAVA讨论', '2');
INSERT INTO `group` VALUES ('2', '快乐大本营', '1');

-- ----------------------------
-- Table structure for group_user
-- ----------------------------
DROP TABLE IF EXISTS `group_user`;
CREATE TABLE `group_user` (
  `gu_id` int(11) NOT NULL AUTO_INCREMENT,
  `g_id` int(11) DEFAULT NULL COMMENT '群ID',
  `u_id` int(11) DEFAULT NULL COMMENT '用户ID',
  `u_type` int(2) DEFAULT NULL COMMENT '是否退群0:已退；1:在群',
  PRIMARY KEY (`gu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of group_user
-- ----------------------------
INSERT INTO `group_user` VALUES ('1', '1', '1', '1');
INSERT INTO `group_user` VALUES ('2', '1', '3', '1');
INSERT INTO `group_user` VALUES ('3', '2', '2', '1');
INSERT INTO `group_user` VALUES ('4', '2', '1', '1');

-- ----------------------------
-- Table structure for relational_users
-- ----------------------------
DROP TABLE IF EXISTS `relational_users`;
CREATE TABLE `relational_users` (
  `r_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `u_id_n` int(11) DEFAULT NULL,
  `r_type` int(11) DEFAULT NULL COMMENT '用户关系是否生效0为不生效',
  PRIMARY KEY (`r_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of relational_users
-- ----------------------------
INSERT INTO `relational_users` VALUES ('1', '1', '2', '1');
INSERT INTO `relational_users` VALUES ('2', '1', '3', '1');
INSERT INTO `relational_users` VALUES ('3', '2', '3', '1');

-- ----------------------------
-- Table structure for u_user
-- ----------------------------
DROP TABLE IF EXISTS `u_user`;
CREATE TABLE `u_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_name` varchar(20) DEFAULT NULL COMMENT '用户名',
  `u_pwd` varchar(100) DEFAULT NULL COMMENT '用户密码',
  `phone` varchar(20) DEFAULT NULL COMMENT '用户手机',
  `u_sex` int(2) DEFAULT NULL COMMENT '性别; 0:女，1:男',
  `u_url` varchar(255) DEFAULT NULL COMMENT '用户头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of u_user
-- ----------------------------
INSERT INTO `u_user` VALUES ('1', 'admin', '123456', '13855556666', '0', 'http://pic.gzpinda.com/allimg/1706/41-1F61G61527.jpg');
INSERT INTO `u_user` VALUES ('2', 'lishi', '123456', '18899998888', '1', 'https://i03piccdn.sogoucdn.com/944c171aff40fcef');
INSERT INTO `u_user` VALUES ('3', 'zhangsan', '123456', '98494894998', '1', 'https://i03piccdn.sogoucdn.com/944c171aff40fcef');
